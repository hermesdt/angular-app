'use strict';

angular.module('beruby')
  .factory('OneSignalService', function ($window) {
    var service = {
        pushTags: function(data){
            $window.plugins.OneSignal.sendTags(data);
        }
    };

    return service;
});