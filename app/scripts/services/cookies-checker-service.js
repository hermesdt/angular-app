'use strict';

angular.module('beruby')
	.service('CookiesCheckerService', function($q, $cookies) {

	var CookiesChecker = function(){
	};

	CookiesChecker.prototype.checkCookies = function(){
		var deferred = $q.defer();

		var self = this;
		self.checkLocalCookies(deferred);
		self.checkThirdPartyCookies(deferred);

		return deferred.promise;
	};

	CookiesChecker.prototype.checkLocalCookies = function(deferred){
		var date = new Date();
		$cookies.put('somecookie', date.getTime());
		if($cookies.get('somecookie') !== date.getTime()){
			deferred.reject('localCookies');
		}

		$cookies.remove('somecookie');

		return deferred;
	};

	CookiesChecker.prototype.checkThirdPartyCookies = function(deferred){
		var image = new Image();
		image.onerror = function(){
			deferred.reject('connectionError');
		};

		image.onload = function(){
			var image2 = new Image();
			image2.onerror = function(){
				deferred.reject('thirdCookies');
			};

			image2.onload = function(){
				deferred.resolve(true);
			};

			image2.src = 'http://es.mirubi.com/check_cookie';
		};
		image.src = 'http://es.mirubi.com/set_cookie';
	};

	return new CookiesChecker();
});
