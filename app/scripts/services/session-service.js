'use strict';

angular.module('beruby')
  .factory('SessionService', function ($window) {

  	var loggedIn = false;

  	return {
  		isLoggedIn: function(){
        if(!loggedIn){
          loggedIn = $window.localStorage.loggedIn === 'yes';
        }

  			return loggedIn;
  		}, setLoggedIn: function(state){
        $window.localStorage.loggedIn = state ? 'yes' : 'no';
  			loggedIn = state;
  		}, getCountry: function(){
        return $window.localStorage.country;
      }, setCountry: function(country){
        $window.localStorage.country = country;
      }
  	};
});