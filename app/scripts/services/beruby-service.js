'use strict';

angular.module('beruby')
  .factory('BerubyService', function ($http, $location, $q, SessionService) {

    function getBaseUrl(country){
      if(country === undefined){
        country = SessionService.getCountry();
        try{ga('set', 'dimension1', country);}catch(e){console.log(e);}
      }

      if($location.$$host === 'localhost' && false){
        return 'http://es.beruby.dev:3003';
      }else{
        return 'http://' + country + '.beruby.com';
      }
    }

  	var service = {
      signup: function(userData){
        var deferred = $q.defer();
        var country = userData.country;
        if(country === undefined){
          deferred.reject();
          return;
        }

        delete userData.country;
        var birthDate = userData.user.birth_date;
        if(birthDate !== undefined){
          if(typeof birthDate === 'string'){
            var tokens = birthDate.split('-');
            delete userData.user.birth_date;
            userData.user['birth_date(1i)'] = tokens[0];
            userData.user['birth_date(2i)'] = tokens[1];
            userData.user['birth_date(3i)'] = tokens[2];
          }else if(typeof birthDate === 'object'){
            userData.user['birth_date(1i)'] = birthDate.getFullYear();
            userData.user['birth_date(2i)'] = birthDate.getMonth();
            userData.user['birth_date(3i)'] = birthDate.getDate();
          }
        }

        $http.get(getBaseUrl(country) + '/users/token').success(function(data){
          userData.authenticity_token = data.token;

          $http.post(getBaseUrl(country) + '/users',
            userData).success(function(data){
              SessionService.setCountry(country);
              SessionService.setLoggedIn(true);

              return deferred.resolve(data);
            }).error(function(reason){
              return deferred.reject(reason);
            });

        }).error(function(reason){
          return deferred.reject(reason);
        });

        return deferred.promise;
      },
  		login: function(country, email, password){
        var deferred = $q.defer();
        var base = getBaseUrl(country);
        var path = '/account/simple_login';
        if(!base.match(/es.beruby.dev/)){
          base = base.replace(/http/, 'https');
          path = '/account/simple_login_ssl?timestamp=1';
        }

        if(country === undefined){
          deferred.reject();
          return;
        }

  			$http.post(base + path,
  				{email: email, password: password}, {cache: false}).success(function(data){
            SessionService.setCountry(country);
            SessionService.setLoggedIn(true);

  					return deferred.resolve(data);
  				}).error(function(reason){
            return deferred.reject(reason);
          });

  				return deferred.promise;
      }, getTransactions: function(page){
        var deferred = $q.defer();
        var base = getBaseUrl();
        if(!base.match(/es.beruby.dev/)){
          base = base.replace(/http/, 'https');
        }

        $http.get(base + '/users/transactions', {params: {page: page}}).success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });
          
        return deferred.promise;
      }, getWidgets: function(){
        var deferred = $q.defer();
        $http.get(getBaseUrl() + '/api/mobile_widgets?format=json').success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });
          
        return deferred.promise;  
      }, search: function(query, page){
        var deferred = $q.defer();
        $http.get(getBaseUrl() + '/portal/search',
          {params: {search: query, target: 'widgets', page: page}}).success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });

          return deferred.promise;
      }, getBalance: function(){
        var deferred = $q.defer();
        var base = getBaseUrl();
        if(!base.match(/es.beruby.dev/)){
          base = base.replace(/http/, 'https');
        }

        $http.get(base + '/users/balance').success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });

          return deferred.promise;
      }, getWidget: function(widgetId){
        var deferred = $q.defer();
        
        $http.get(getBaseUrl() + '/advertisers/advertiser_info/' + widgetId).success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });

          return deferred.promise;
      }, getCategoriesForSection: function(section){
        var path = null;
        if(section === 'shopping'){
          path = '/advertisers/mobile_shopping_sections'; 
        }else if(section === 'travels'){
          path = '/advertisers/mobile_travel_sections';
        }else if(section === 'offers'){
          path = '/advertisers/mobile_offers_sections';
        }
        var deferred = $q.defer();
        $http.get(getBaseUrl() + path).success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });

          return deferred.promise;
      }, getWidgetsForUrl: function(url, page){
        var deferred = $q.defer();
        $http.get(url, {params: {page: page}}).success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });

          return deferred.promise;
      }, createClickAccount: function(widgetId){
        var deferred = $q.defer();
        
        $http.get(getBaseUrl() + '/portal/mobile_click_account',
            {params: {widget_id: widgetId}})
          .success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });

          return deferred.promise;
  		}, getOTP: function(){
        var deferred = $q.defer();
        
        $http.get(getBaseUrl() + '/account/generate_otp')
          .success(function(data){
            return deferred.resolve(data);
          }).error(function(reason){
            return deferred.reject(reason);
          });

          return deferred.promise;
      }, getUserInfo: function(){
        var deferred = $q.defer();

        $http.get(getBaseUrl() + '/account/info').success(function(data){
          return deferred.resolve(data);
        }).error(function(reason){
          return deferred.reject(reason);
        });

        return deferred.promise;
      }
  	};

  	return service;
  });