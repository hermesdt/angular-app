'use strict';

angular.module('beruby')
	.service('NavigationService', function($rootScope, $location, $timeout) {
		this.stack = [];
		this.path = function(path, truncate) {
			if (truncate) {
				this.clear();
			}

			this.stack.push(['path', path]);
			try{
				ga('send', 'pageview', path);
			}catch(e){
				console.log(e);
				$timeout(function(){
			    	try{ga('send', 'pageview', path);}catch(e){console.log(e);}
			    }, 4000);
			}
			$location.path(path);
		};

		this.url = function(url, truncate) {
			if (truncate) {
				this.clear();
			}

			this.stack.push(['url', url]);
			try{
				ga('send', 'pageview', url);
			}catch(e){
				console.log(e);
				$timeout(function(){
			    	try{ga('send', 'pageview', url);}catch(e){console.log(e);}
			    }, 4000);
			}
			$location.url(url);
		};

		this.clear = function() {
			this.stack.length = 0;
		};

		this.back = function() {
			if (this.stack.length > 1) {
				this.stack.pop();
				var newLocation = this.stack.pop();				
				switch (newLocation[0]) {
					case 'url':
						this.url(newLocation[1]);
						break;
					case 'path':
						this.path(newLocation[1]);
						break;
				}

				return true;
			} else {
				return false;
			}
		};
	});