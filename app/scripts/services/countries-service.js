'use strict';

angular.module('beruby')
  .constant('Countries',
    ['es-design', 'it', 'pt', 'br', 'mx', 'ar', 'co', 'cl', 'us', 'tr']
  );