'use strict';

angular.module('beruby')
  .config(function($translateProvider) {
  	$translateProvider.translations('en', {
      search: {
        placeholder: 'Search shops',
        tutorial:  'Search for a store!<br /><br /> Or explore our directory to earn cashback in youn next purchase or travel booking',
        empty: 'Unfortunately we cannot find what you are looking for!  Try to search in our directory'

      },
      sections: {
        balance: 'balance',
        offers: 'offers',
        travels: 'travel',
        shopping: 'shops'
      },
      welcome: {
        start: 'start',
        shopping_title: 'shops',
        travels_title: 'travel',
        offers_title: 'offers',
        shopping_phrase: 'We refund you part of the value of your purchases<br />'+
          '<b>More than 500 online stores</b> to buy.',
        travels_phrase: 'We refund you part of the value of your travel arrangements<br />'+
          '<b>Book through beruby</b> your plane, hotel, car...',
        offers_phrase: 'We refund you part of the value of your purchases<br />'+
          '<b>More than 500 online stores</b> to shop.',
      },
      form: {
        country: 'Country',
        email: 'Email',
        password: 'Password',
        enter: 'Enter',
        no_account: '<b>Need an account?</b><br />Register now and start saving!',
        signup: 'Sign up',
        birth_date: 'Date of birth',
        gender: 'Gender',
        name: 'Name',
        man: 'Male',
        woman: 'Female',
        countries: {
          es: 'España',
          uk: 'United Kingdom',
          us: 'United States',
          pt: 'Portugal',
          br: 'Brasil',
          co: 'Colombia',
          cl: 'Chile',
          ar: 'Argentina',
          mx: 'Mexico',
          it: 'Italia',
          nl: 'Netherland',
          tr: 'Türkiye',
          de: 'Deutschland',
          fr: 'France',
          america: 'America'
        },
        login_error: 'Invalid username or password'
      },
      balance: {
        unconfirmed: 'Unconfirmed balance',
        confirmed: 'Confirmed balance',
        direct: 'Direct',
        network: 'My network',
        transactions: 'Transactions',
        share: 'Share Beruby with your friends'
      },
      widget: {
        visit: 'Visit',
        particular_conditions: 'Particular conditions',
        last_launch_date: 'Last commission',
        last_paid_date: 'Last Payment',
        related: 'Related merchants',
        monetize: 'How to earn cashback',
        products: 'Products and Services',
        concept: 'Action',
        cashback: 'Cashback',
        direct_amount: 'You',
        network_amount: 'Your Network',
        checking_cookies: 'One moment, please...',
        no_cookies: 'Important! Cookies are not enabled in your browser so your purchases will not be tracked.  <a href="http://{{country}}.beruby.com/web/cookies" target="_blank">More info.</a>',
        warning: 'Warning'
      },
      transactions: {
        empty: 'You haven\'t had any activity in the last three months'
      }
    });

    $translateProvider.translations('it', {
      search: {
        placeholder: 'Cercare negozi',
        tutorial: 'Cerca un negozio!<br /><br /> O esplora le nostre categorie per generare cashback con il tuo prossimo acquisto o prenotazione',
        empty: 'Peccato! Non troviamo quello che cerchi! Prova a cercare nelle nostre categorie'
      },
      sections: {
        balance: 'saldo',
        offers: 'offerte',
        travels: 'viaggi',
        shopping: 'acquisti'
      },
      welcome: {
        start: 'iniziare',
        shopping_title: 'acquisti',
        travels_title: 'viaggi',
        offers_title: 'offerte',
        shopping_phrase: 'Ti rimborsiamo una parte del valore dei tuoi acquisti<br />' +
          '<b>Oltre di 600 negozi online</b> in cui comprare.',
        travels_phrase: 'Ti rimborsiamo una parte del valore dei tuoi viaggi<br />' +
          '<b>Prenota attraverso beruby</b> il tuo volo, hotel, automobile, etc.',
        offers_phrase: 'Ti rimborsiamo una parte del valore dei tuoi acquisti<br />' +
          '<b>Oltre di 600 negozi online</b> in cui comprare.',
      },
      form: {
        country: 'Paese',
        email: 'Email',
        password: 'Password',
        enter: 'Entrare',
        no_account: '<b>¿Non hai un account?</b><br />Registrati e comincia a risparmiare!',
        signup: 'Regístrati',
        birth_date: 'Data di nascita',
        gender: 'Sesso',
        name: 'Nome',
        man: 'Uomo',
        woman: 'Donna',
        countries: {
          es: 'España',
          uk: 'United Kingdom',
          us: 'United States',
          pt: 'Portugal',
          br: 'Brasil',
          co: 'Colombia',
          cl: 'Chile',
          ar: 'Argentina',
          mx: 'Mexico',
          it: 'Italia',
          nl: 'Netherland',
          tr: 'Türkiye',
          de: 'Deutschland',
          fr: 'France',
          america: 'America'
        },
        login_error: 'Utente o password non validi'
      },
      balance: {
        unconfirmed: 'Saldo da confermare',
        confirmed: 'Saldo confermato',
        direct: 'Diretto',
        network: 'La mia rete',
        transactions: 'Transazioni',
        share: 'Condividi beruby con i toi amici'
      },
      widget: {
        visit: 'Visitare',
        particular_conditions: 'Condizioni particolari',
        last_launch_date: 'Ultima commissione',
        last_paid_date: 'Ultimo pagamento',
        related: 'inserzionisti Simili',
        products: 'Prodotti e servizi',
        monetize: 'Come risparmiare',
        concept: 'Concetto',
        cashback: 'Cashback',
        direct_amount: 'Tu',
        network_amount: 'La tua rete',
        checking_cookies: 'Un momento per favore...',
        no_cookies: 'Importante! I cookie del tuo browser non sono attivati e i tuoi acquisti non saranno registrati. <a href="http://{{country}}.beruby.com/web/cookies" target="_blank">Ulteriori informazioni.</a>',
        warning: 'Warning'
      },
      transactions: {
        empty: 'Negli ultimi tre mesi non hai richiesto nessun pagamento'
      }
    });

    $translateProvider.translations('br', {
      search: {
        placeholder: 'Procurar lojas',
        tutorial: 'Procurar uma loja!<br /><br /> Ou navegue pelas nossas categorias para gerar um cashback nas sua próxima compra ou reserva',
        empty: 'Que pena! Não encontramos o que você procura! Tente procurar em nossas categorias'
      },
      sections: {
        balance: 'saldo',
        offers: 'ofertas',
        travels: 'viagens',
        shopping: 'compras'
      },
      welcome: {
        start: 'come&ccedil;ar',
        shopping_title: 'compras',
        travels_title: 'viagens',
        offers_title: 'ofertas',
        shopping_phrase: 'Lhe reembolsamos parte do valor das suas compras<br />' +
          '<b>Mais de 250 lojas online</b> onde comprar.',
        travels_phrase: 'Lhe reembolsamos parte do valor das suas viagens<br />' +
          '<b>Reserve atrav&eacute;s de beruby</b> o seu voo, hotel, carro, etc.',
        offers_phrase: 'Lhe reembolsamos parte do valor das suas compras<br />' +
          '<b>Mais de 250 lojas online</b> onde comprar.',
      },
      form: {
        contry: 'País',
        email: 'Email',
        password: 'Senha',
        enter: 'Entrar',
        no_account: '<b>N&atilde;o tem uma conta?</b><br />Registre-se j&aacute; e comece a economizar!',
        signup: 'Registre-se',
        birth_date: 'Data de nascimento',
        gender: 'Sexo',
        name: 'Nome',
        man: 'Homem',
        woman: 'Mulher',
        countries: {
          es: 'España',
          uk: 'United Kingdom',
          us: 'United States',
          pt: 'Portugal',
          br: 'Brasil',
          co: 'Colombia',
          cl: 'Chile',
          ar: 'Argentina',
          mx: 'Mexico',
          it: 'Italia',
          nl: 'Netherland',
          tr: 'Türkiye',
          de: 'Deutschland',
          fr: 'France',
          america: 'America'
        },
        login_error: 'Us&aacute;rio ou senha inv&aacute;lido(s)'
      },
      balance: {
        unconfirmed: 'Saldo por confirmar',
        confirmed: 'Saldo confirmado',
        direct: 'Direto',
        network: 'Minha Rede',
        transactions: 'Transa&ccedil;&otilde;es',
        share: 'Partilha beruby com os teus amigos'
      },
      widget: {
        visit: 'Visitar',
        particular_conditions: 'Condi&ccedil;&otilde;es particulares',
        last_launch_date: '&Uacute;ltima comiss&atilde;o',
        last_paid_date: '&Uacute;ltimo pagamento',
        related: 'Anunciantes relacionados',
        monetize: 'Como ganhar dinheiro',
        products: 'Produtos e serviços',
        concept: 'Conceito',
        cashback: 'Cashback',
        direct_amount: 'Você',
        network_amount: 'Sua Rede',
        checking_cookies: 'Um momento por favor',
        no_cookies: 'Atenção! Os cookies do seu navegador não estão ativados, por esta razão as compras não serão registradas. <a href="http://{{country}}.beruby.com/web/cookies" target="_blank">Mais informações.</a>',
        warning: 'Warning'
      },
      transactions: {
        empty: 'Não há atividade nesta conta nos últimos três meses'
      }
    });

    $translateProvider.translations('pt', {
      search: {
        placeholder: 'Procurar lojas',
        tutorial: 'Procurar uma loja!<br /><br /> Ou navega pelas nossas categorias para gerar um cashback nas tua próxima compra ou reserva',
        empty: 'Que pena! Não encontramos o que procuras! Tenta procurar em nossas categorias'
      },
      sections: {
        balance: 'saldo',
        offers: 'ofertas',
        travels: 'viagens',
        shopping: 'compras'
      },
      welcome: {
        start: 'come&ccedil;ar',
        shopping_title: 'compras',
        travels_title: 'viagens',
        offers_title: 'ofertas',
        shopping_phrase: 'Reembolsamos-te parte do valor das tuas compras<br />' +
          '<b>Mais de 200 lojas online</b> onde comprar.',
        travels_phrase: 'Reembolsamos-te parte do valor das tuas viagens<br />' +
          '<b>Reserva atrav&eacute;s de beruby</b> o teu voo, hotel, carro, etc.',
        offers_phrase: 'Reembolsamos-te parte do valor das tuas compras<br />' +
          '<b>Mais de 200 lojas online</b> onde comprar.',
      },
      form: {
        contry: 'País',
        email: 'Email',
        password: 'Password',
        enter: 'Entrar',
        no_account: '<b>N&atilde;o tens uma conta?</b><br />Registra-te j&aacute; e come&ccedil; a poupar!',
        signup: 'Registra-te',
        birth_date: 'Data de nascimento',
        gender: 'Sexo',
        name: 'Nome',
        man: 'Homem',
        woman: 'Mulher',
        countries: {
          es: 'España',
          uk: 'United Kingdom',
          us: 'United States',
          pt: 'Portugal',
          br: 'Brasil',
          co: 'Colombia',
          cl: 'Chile',
          ar: 'Argentina',
          mx: 'Mexico',
          it: 'Italia',
          nl: 'Netherland',
          tr: 'Türkiye',
          de: 'Deutschland',
          fr: 'France',
          america: 'America'
        },
        login_error: 'Usu&aacute;rio ou password inv&aacute;lido(s)'
      },
      balance: {
        unconfirmed: 'Saldo por confirmar',
        confirmed: 'Saldo confirmado',
        direct: 'Directo',
        network: 'Minha Rede',
        transactions: 'Transac&ccedil;&otilde;es',
        share: 'Partilha beruby com os teus amigos'
      },
      widget: {
        visit: 'Visitar',
        particular_conditions: 'Condi&ccedil;&otilde;es particulares',
        last_launch_date: '&Uacute;ltima comiss&atilde;o',
        last_paid_date: '&Uacute;ltimo pagamento',
        related: 'Anunciantes relacionados',
        monetize: 'Como poupar dinheiro',
        products: 'Produtos e serviços',
        concept: 'Conceito',
        cashback: 'Cashback',
        direct_amount: 'Tu',
        network_amount: 'A tua Rede',
        checking_cookies: 'Um momento por favor... ',
        no_cookies: 'Atenção! Os cookies do teu navegador não estão activados, por esta razão as compras não serão registadas. <a href="http://{{country}}.beruby.com/web/cookies" target="_blank">Mais informações.</a>',
        warning: 'Warning'
      },
      transactions: {
        empty: 'Não existe actividade nesta conta nos últimos três meses'
      }
    });

    $translateProvider.translations('tr', {
      search: {
        placeholder: 'Mağaza ara',
        tutorial: 'Sevdiğin mağazayı ara!<br /><br /> Ya da bir sonraki alışveriş ya da rezervasyonunuzda para iadesi kazanmak için kategorilerimizi gez',
        empty: 'Uff! Aradığını bir türlü bulamıyoruz! Bir de kategorilerimizi inceleyerek bulmayı dene'
      },
      sections: {
        balance: 'bakiye',
        offers: 'fırsatlar',
        travels: 'seyahat',
        shopping: 'alışveriş'
      },
      welcome: {
        start: 'alışverişe başla',
        shopping_title: 'alışveriş',
        travels_title: 'seyahat',
        offers_title: 'fırsatlar',
        shopping_phrase: 'Harcamalarının bir kısmını sana iade ediyoruz<br />' +
          'Alışveriş yapabileceğin <b>300+ online mağaza</b>',
        travels_phrase: 'Seyahat harcamalarının bir kısmını sana iade ediyoruz<br />' +
          '<b>beruby üzerinden</b> uçak bileti al, otel rezervasyonu yap, araç kirala',
        offers_phrase: 'Harcamalarının bir kısmını sana iade ediyoruz<br />' +
          'Alışveriş yapabileceğin <b>300+ online mağaza</b> ',
      },
      form: {
        contry: 'Ülke',
        email: 'Email',
        password: 'Şifre',
        enter: 'giriş',
        no_account: '<b>beruby hesabın yok mu?</b><br />Hemen üye ol ve kazanmaya başla!',
        signup: 'Üye ol',
        birth_date: 'Doğum Tarihi',
        gender: 'Cinsiyet',
        name: 'İsim',
        man: 'Erkek',
        woman: 'Kadın',
        countries: {
          es: 'España',
          uk: 'United Kingdom',
          us: 'United States',
          pt: 'Portugal',
          br: 'Brasil',
          co: 'Colombia',
          cl: 'Chile',
          ar: 'Argentina',
          mx: 'Mexico',
          it: 'Italia',
          nl: 'Netherland',
          tr: 'Türkiye',
          de: 'Deutschland',
          fr: 'France',
          america: 'America'
        },
        login_error: 'Kullanıcı adı ya da şifre yanlış'
      },
      balance: {
        unconfirmed: 'Onay bekleyen bakiye',
        confirmed: 'Onaylanmış bakiye',
        direct: 'Direkt',
        network: 'Ağım',
        transactions: 'İşlemlerim',
        share: 'beruby´yi arkadaşlarına öner'
      },
      widget: {
        visit: 'Ziyaret et',
        particular_conditions: 'Özel durumlar',
        last_launch_date: 'Son komisyon',
        last_paid_date: 'Son ödeme',
        related: 'İlgili mağazalar',
        products: 'Ürün ve Hizmetler',
        monetize: 'Nasıl para kazanırım',
        concept: 'Kavram',
        cashback: 'Para iadesi',
        direct_amount: 'Sen',
        network_amount: 'Ağınız',
        checking_cookies: 'Bir dakika lütfen...',
        no_cookies: 'Önemli! Çerez ayarların etkin değil bu yüzden alışverişlerin işlenmeyecek! <a href="http://{{country}}.beruby.com/web/cookies" target="_blank">Daha fazla bilgi</a>',
        warning: 'Warning'
      },
      transactions: {
        empty: 'Son üç aydır hesabında bir hareket yok'
      }
    });

  	$translateProvider.translations('es', {
  		search: {
        placeholder: 'Buscar tiendas',
        tutorial: '¡Busca una tienda!<br /><br /> O explora nuestras categorías para generar cashback en tu próxima compra o reserva',
        empty: '¡Qué pena! ¡No encontramos lo que buscas! Prueba a buscar en nuestras categorías'
      },
      sections: {
        balance: 'saldo',
        offers: 'ofertas',
        travels: 'viajes',
        shopping: 'compras'
      },
      welcome: {
        start: 'empezar',
        shopping_title: 'compras',
        travels_title: 'viajes',
        offers_title: 'ofertas',
        shopping_phrase: 'Te reembolsamos parte del valor de tus compras<br />' +
          '<b>M&aacute;s de 600 tiendas online</b> en las que comprar.',
        travels_phrase: 'Te reembolsamos parte del valor de tus viajes<br />' +
          '<b>Reserva a trav&eacute;s de beruby</b> tu avi&oacute;n, hotel, coche, etc.',
        offers_phrase: 'Otras formas <b>de ahorrar dinero</b><br />' +
          '(visitas, cupones, registros, vídeos...)',
      },
      form: {
        country: 'País',
        email: 'Email',
        password: 'Contraseña',
        enter: 'Entrar',
        no_account: '<b>¿No tienes una cuenta?</b><br />Reg&iacute;strate ya y ¡empieza a ahorrar!',
        signup: 'Regístrate',
        birth_date: 'Fecha de nacimiento',
        gender: 'Sexo',
        name: 'Nombre',
        man: 'Hombre',
        woman: 'Mujer',
        countries: {
          es: 'España',
          uk: 'United Kingdom',
          us: 'United States',
          pt: 'Portugal',
          br: 'Brasil',
          co: 'Colombia',
          cl: 'Chile',
          ar: 'Argentina',
          mx: 'Mexico',
          it: 'Italia',
          nl: 'Netherland',
          tr: 'Türkiye',
          de: 'Deutschland',
          fr: 'France',
          america: 'America'
        },
        login_error: 'Usuario o contraseña inválido(s)'
      },
      balance: {
        unconfirmed: 'Saldo por confirmar',
        confirmed: 'Saldo confirmado',
        direct: 'Directo',
        network: 'Mi Red',
        transactions: 'Transacciones',
        share: 'Comparte Beruby con tus amigos'
      },
      widget: {
        visit: 'Visitar',
        particular_conditions: 'Condiciones particulares',
        last_launch_date: 'Última comisión',
        last_paid_date: 'Último pago',
        related: 'Anunciantes relacionados',
        products: 'Productos y servicios',
        monetize: 'Cómo ganar dinero',
        concept: 'Concepto',
        cashback: 'Cashback',
        direct_amount: 'Tú',
        network_amount: 'Tu Red',
        checking_cookies: 'Un momento por favor... ',
        no_cookies: '¡Importante! Las cookies de tu navegador no están activadas por lo que las compras no se computarán. <a href="http://{{country}}.beruby.com/web/cookies" target="_blank">Más información.</a>',
        warning: 'Utilizar la app de la tienda en vez del navegador de tu móvil podrá hacer que pierdas tu cashback'
      },
      transactions: {
        empty: 'No has tenido actividad en la cuenta en los últimos tres meses'
      }
	  });

    var language = navigator.language.split('-')[0];
    try{
      $translateProvider.use(language);
    }catch(e){
      $translateProvider.use('es');
    }
  });