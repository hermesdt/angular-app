'use strict';

angular.module('beruby', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngTouch', 'ngRoute', 'pascalprecht.translate', 'infinite-scroll'])
  .config(function($routeProvider, $httpProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'partials/welcome.html',
        controller: 'WelcomeController'
      })
      .when('/login', {
        templateUrl: 'partials/login.html',
        controller: 'LoginController'
      })
      .when('/signup', {
        templateUrl: 'partials/signup.html',
        controller: 'SignupController'
      })
      .when('/search', {
        templateUrl: 'partials/search.html',
        controller: 'SearchController'
      })
      .when('/balance', {
        templateUrl: 'partials/balance.html',
        controller: 'BalanceController'
      })
      .when('/widgets/:id', {
        templateUrl: 'partials/show_widget.html',
        controller: 'ShowWidgetController'
      })
      .when('/widgets', {
        templateUrl: 'partials/widgets_section.html',
        controller: 'WidgetsSectionController'
      })
      .when('/category', {
        templateUrl: 'partials/category.html',
        controller: 'CategoryController'
      })
      .when('/transactions', {
        templateUrl: 'partials/transactions.html',
        controller: 'TransactionsController'
      })
      .otherwise({
        redirectTo: '/'
      });

    $httpProvider.defaults.withCredentials = true;
    $httpProvider.defaults.headers.common['X-MobileVersion'] = '2.1';
    $httpProvider.defaults.cache = false;

    $httpProvider.interceptors.push(function($q, SessionService, NavigationService) {
      return {
        responseError: function(rejection) {
          if (rejection.status === 401) {
            SessionService.setLoggedIn(false);
            NavigationService.path('/login', true);
          }

          return $q.reject(rejection);
        }
      };
    });
    
  }).run(function($rootScope){
    $rootScope.$on('$routeChangeSuccess', function () {
        jQuery(document).foundation('reflow');
    });
  }).run(function($window, NavigationService){
    document.addEventListener('deviceready', function () {
      // Enable to debug issues.
      // $window.plugins.OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});
  
      var notificationOpenedCallback = function(jsonData) {
        console.log('didReceiveRemoteNotificationCallBack: ' + JSON.stringify(jsonData));
        NavigationService.path('/notifications/' + $window.encodeURIComponent(jsonData.additionalData.url));
      };

      //AIzaSyDe50ODkPrWlrpInIaCT6cVZzv95Vdz4rI google gcm api key
      $window.plugins.OneSignal.init(
        'af048a6d-43a6-40b9-9d31-f37448080c04', {googleProjectNumber: '719929336035'},
        notificationOpenedCallback);

      // Show an alert box if a notification comes in when the user is in your app.
      $window.plugins.OneSignal.enableInAppAlertNotification(false);
      $window.plugins.OneSignal.enableNotificationsWhenActive(true);
    }, false);
  }).run(function($rootScope, $location, SessionService, NavigationService, Countries) {
    $rootScope.loadPath = function(path, truncate) {
      NavigationService.url(path, truncate);
    };

    document.addEventListener('deviceready', function(){
      document.addEventListener('backbutton', function(){
        $rootScope.$apply(function(){
          if(!NavigationService.back() && navigator && navigator.app){
            navigator.app.exitApp();
          }
        });
      }, false);
    }, false);

    $rootScope.navigation = {};

    $rootScope.header = {
      image: 'beruby_red.png',
      section: '',
      back: function(){
        NavigationService.back();
      }
    };
    $rootScope.countries = Countries;

    $rootScope.navigateBack = false;
    if(navigator && navigator.userAgent && !navigator.userAgent.match(/android/i)){
      $rootScope.platform = 'ios';
    }
    $rootScope.loading = false;

    $rootScope.layout = {
      bodyBackground: ''
    };

    if (SessionService.isLoggedIn()) {
      NavigationService.url('/search', true);
    } else {
      NavigationService.path('/', true);
    }
  });