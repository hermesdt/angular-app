'use strict';

angular.module('beruby')
  .controller('ShowWidgetController', function ($scope, $route, $routeParams, $window, $translate, SessionService, BerubyService) {
    $scope.loading = true;
    $scope.navigateBack = true;
    
    var widgetId = $routeParams.id;
    $scope.widget = {
      section: $routeParams.section,
    };

    function cleanNewLines(text){
      if(text && text.length > 0){
        return text.replace(/^- /, '')
          .replace(/\r\n/g, '\n')
          .replace(/\n-/g, '\n')
          .replace(/\n/g, '<br />');
      }
    }

    BerubyService.getWidget(widgetId).then(function(data){
      $scope.widget.name = data.name;
      $scope.widget.payment = data.payment;
      $scope.widget.imagePath = data.image_path;
      $scope.widget.summary = cleanNewLines(data.desc_summary);
      $scope.widget.products = cleanNewLines(data.desc_products);
      $scope.widget.monetize = cleanNewLines(data.desc_monetize);
      $scope.widget.url = data.url;
      $scope.widget.moreInfo = data.more_info;
      $scope.widget.lastPaidDate = data.last_paid_date;
      $scope.widget.lastLaunchDate = data.last_launch_date;
      $scope.widget.related = cleanNewLines(data.related);
      $scope.widget.particularConditions = cleanNewLines(data.particular_conditions);
      $scope.widget.particularConditions = cleanNewLines(data.particular_conditions);
      $scope.widget.commission = data.commission;
      $scope.widget.earnings = data.earnings;
      $scope.widget.type = data.type;
      $scope.widget.earningsTitle = data.earnings_title;
        

    }).finally(function(){
      $scope.loading = false;
    });

    $scope.visit = function(){
      BerubyService.getOTP().then(function(otp){
        BerubyService.createClickAccount(widgetId).finally(function(){
          var url = $scope.widget.url;
          var match = url.match(/(.*)otp=.*return_to(.*)/);
          var newUrl = match[1] + 'otp=' + otp.code + '&return_to' + match[2];

          try{ga('send', 'pageview', '/visit/' + widgetId);}catch(e){console.log(e);}
          $window.open(newUrl, '_system');
        });
      });
    };
  });
