'use strict';

angular.module('beruby')
  .controller('SearchController', function ($scope, $routeParams, BerubyService, OneSignalService, NavigationService) {
  	var query = null;
  	var nextPage = 1;
    var hasMorePages = true;
  	$scope.navigation.active = 'search';
  	$scope.search = {
  		widgets: [],
      fetchingPage: false,
      empty: false,
      hideMessage: false
  	};
    $scope.header.section = '';
    
    BerubyService.getUserInfo().then(function(data){
      OneSignalService.pushTags(data);
    }).catch(function(){});

    $scope.search.submit = function(){
      if($scope.query !== undefined){
        NavigationService.url('/search?query=' + $scope.query);
      }
    };

    function appendWidgets(widgets){
      widgets.forEach(function(widget){
        $scope.search.widgets.push(widget);
      });
    }

    $scope.fetchNextPage = function(){
      if(query === undefined || !hasMorePages || $scope.search.fetchingPage){
        return;
      }

      $scope.search.fetchingPage = true;
      BerubyService.search(query, nextPage)
        .then(function(data){
          if(data.widgets.length > 0){
            $scope.search.empty = false;
            appendWidgets(data.widgets);
          }else{
            $scope.search.empty = true;
          }

          if(data.next_page !== null){
            nextPage = data.next_page;
          }else{
            hasMorePages = false;
          }

        }).catch(function(){
          hasMorePages = false;
        }).finally(function(){
          $scope.search.fetchingPage = false;
        });
    };

    query = $routeParams.query;
    if(query !== undefined){
      $scope.search.hideMessage = true;
      $scope.fetchNextPage();
      $scope.query = query;
      $scope.navigateBack = true;
    }




    $scope.homeWidgets = [];
    var page = 0;
    var haveMorePages = true;
    var waitingForQuery = false;

    function appendHomeWidgets(homeWidgets){
      homeWidgets.forEach(function(homeWidget){
         $scope.homeWidgets.push(homeWidget);
      });
    }

    function fetchNextPage(){
      if(haveMorePages === false || waitingForQuery){
        return;
      }

      $scope.fetchingPage = true;
      page += 1;
      waitingForQuery = true;
      BerubyService.getWidgets(page).then(function(data){
        haveMorePages = false;
        if(data.length > 0){
          appendHomeWidgets(data);

        }else{
          haveMorePages = false;
          if(page === 1){
            $scope.homeWidgetsEmpty = true;
          }
        }
      }).finally(function(){
        $scope.loading = false;
        waitingForQuery = false;
        $scope.fetchingPage = false;
      });
    }

    $scope.fetchNextPage = fetchNextPage;
});