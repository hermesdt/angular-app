'use strict';

angular.module('beruby')
  .controller('CategoryController', function ($scope, $routeParams, BerubyService) {
    var hasMorePages = true;
    var nextPage = 1;
    $scope.navigateBack = true;

  	$scope.category = {
  		section: $routeParams.section,
      fetchingPage: false,
      widgets: []
  	};

    function appendWidgets(widgets){
      widgets.forEach(function(widget){
        $scope.category.widgets.push(widget);
      });
    }

    $scope.fetchNextPage = function(){
      if(!hasMorePages || $scope.category.fetchingPage){
        return;
      }

      $scope.category.fetchingPage = true;
      $scope.loading = true;
      BerubyService.getWidgetsForUrl($routeParams.url, nextPage).then(function(data){
        if(data.widgets.length > 0){
          appendWidgets(data.widgets);
          nextPage = data.page + 1;
        }else{
          hasMorePages = false;
        }
      }).catch(function(){
        hasMorePages = false;
      }).finally(function(){
        $scope.loading = false;
        $scope.category.fetchingPage = false;
      });
    };

    $scope.fetchNextPage();
  });