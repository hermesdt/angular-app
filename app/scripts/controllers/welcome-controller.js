'use strict';

angular.module('beruby')
  .controller('WelcomeController', function ($scope, $window, $timeout, NavigationService) {
    if($window.localStorage.welcomeDone === 'yes'){
      NavigationService.path('/login', true);
    }

  	$scope.welcome =  {
      animate: 'animate-left',
  		currentId: 0,
  		slides: [
  			{name: 'shopping', id: 0},
  			{name: 'travels', id: 1},
  			{name: 'offers', id: 2}
  		],
  	};

    $scope.welcome.submit = function(){
      $window.localStorage.welcomeDone = 'yes';
      NavigationService.path('/login', true);
    };

    $scope.welcome.swipeRight = function(){
      $scope.welcome.animate = 'animate-right';
      $scope.welcome.currentId -= 1;
      if($scope.welcome.currentId < 0){
        $scope.welcome.currentId = 0;
      }
    };

    $scope.welcome.swipeLeft = function(){
      $scope.welcome.animate = 'animate-left';
      $scope.welcome.currentId += 1;
      if($scope.welcome.currentId > 2){
        $scope.welcome.currentId = 2;
      }
    };
  });
