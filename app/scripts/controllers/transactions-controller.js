'use strict';

angular.module('beruby')
  .controller('TransactionsController', function ($scope, BerubyService) {
  	$scope.loading = true;
  	$scope.transactions = [];
    $scope.header.image = 'beruby_balance.png';
    $scope.navigateBack = true;
    $scope.transactionsEmpty = false;
    $scope.fetchingPage = false;

    var page = 0;
    var haveMorePages = true;
    var waitingForQuery = false;

    function appendTransactions(transactions){
      transactions.forEach(function(transaction){
        $scope.transactions.push(transaction);
      });
    }

    function fetchNextPage(){
      if(haveMorePages === false || waitingForQuery){
        return;
      }

      $scope.fetchingPage = true;
      page += 1;
      waitingForQuery = true;
      BerubyService.getTransactions(page).then(function(data){
        if(data.transactions.length > 0){
          appendTransactions(data.transactions);
        }else{
          haveMorePages = false;
          if(page === 1){
            $scope.transactionsEmpty = true;
          }
        }
      }).finally(function(){
        $scope.loading = false;
        waitingForQuery = false;
        $scope.fetchingPage = false;
      });
    }

    $scope.fetchNextPage = fetchNextPage;
});