'use strict';

angular.module('beruby')
  .controller('BalanceController', function ($scope,$window, $translate, BerubyService) {
  	$scope.navigation.active = 'balance';
    $scope.header.section = $translate.instant('sections.balance');
  	$scope.balance = {
  		direct: {
  		},
  		network:{
  		} 
  	};



  	$scope.loading = true;
  	BerubyService.getBalance().
  		then(function(data){
  			$scope.balance.network.unconfirmed = data.amount.network[1];
  			$scope.balance.network.confirmed = data.amount.network[2];
  			$scope.balance.direct.unconfirmed = data.amount.direct[1];
  			$scope.balance.direct.confirmed = data.amount.direct[2];
  			$scope.balance.avatarPath = data.avatar_path;
  			$scope.balance.totalAmountText = data.total_amount_text;
  			$scope.balance.totalAmount = data.total_amount;
  			$scope.balance.userName = data.user_name;
  			$scope.balance.promocode = data.promocode_url;

  		}).finally(function(){
  			$scope.loading = false;
  		});

    $scope.open_fb = function(){
      $window.open('http://www.facebook.com/sharer.php?u='+ $scope.balance.promocode + '&t=Beruby', '_system');
    };
    $scope.open_ws = function(){
      $window.open('whatsapp://send?text='+$scope.balance.promocode , '_system');

    };
  });
