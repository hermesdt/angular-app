'use strict';

angular.module('beruby')
  .controller('WidgetsSectionController', function ($scope, $routeParams, $translate, BerubyService) {
    
  	var section = $routeParams.section;
    $scope.header.section = $translate.instant('sections.' + section);
  	$scope.section = {
  		name: section,
  		categories: []
  	};

  	$scope.navigation.active = section;

  	$scope.loading = true;
  	BerubyService.getCategoriesForSection(section).then(function(data){
  		$scope.section.categories = data;
  	}).finally(function(){
		  $scope.loading = false;
  	});

  });