'use strict';

angular.module('beruby')
  .controller('SignupController', function ($scope, BerubyService, NavigationService) {
  	$scope.signup = {
  		errors: {}
  	};

    var signupCountries = [];
    for(var i = 0;i<$scope.countries.length;i++){
      if($scope.countries[i] !== 'uk' &&
        $scope.countries[i] !== 'nl' &&
        $scope.countries[i] !== 'mx' &&
        $scope.countries[i] !== 'ar' &&
        $scope.countries[i] !== 'co' &&
        $scope.countries[i] !== 'cl'){
        signupCountries.push($scope.countries[i]);
      }
    }
    signupCountries.push('america');
    $scope.countries = signupCountries;

  	$scope.signup.submit = function(){
  		BerubyService.signup({
  			user: {
  				email: $scope.signup.email,
  				name: $scope.signup.name,
  				password: $scope.signup.password,
  				birth_date: $scope.signup.birthDate,
  				gender: $scope.signup.gender

  			}, country: $scope.signup.country
  		}).then(function(){
        try{ga('send', 'event', 'signup', 'success');}catch(e){console.log(e);}
        NavigationService.path('/search', true);
  		}).catch(function(reason){
        try{ga('send', 'event', 'signup', 'error');}catch(e){console.log(e);}
  			if(reason.response === 'error'){
  				$scope.signup.errors = reason.errors;
  			}
  		});
  	};
  });
