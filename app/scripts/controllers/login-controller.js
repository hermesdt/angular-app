'use strict';

angular.module('beruby')
  .controller('LoginController', function ($scope, $http, $translate, $timeout, BerubyService, SessionService, NavigationService) {
    if(SessionService.isLoggedIn()){
      NavigationService.path('/search', true);
    }

    $http.get('http://ipinfo.io').success(function(response) {
        var country_code = response.country;
        var index = $scope.countries.indexOf(country_code.toLowerCase()+'-design');
        $scope.login.country = $scope.countries[index];
    });
  
    
  	$scope.login = {
      errors: [],
      loading: false
  	};

    function showError(){
      $scope.login.errors.push('error');
    }
  	$scope.login.submit = function(){
      $scope.login.errors = [];

  		var country = $scope.login.country;
  		var email = $scope.login.email;
  		var password = $scope.login.password;

      $scope.loading = true;
  		BerubyService.login(country, email, password).then(function(){
        SessionService.setLoggedIn(true);
        NavigationService.path('/search', true);
  		}).catch(function(){
        try{ga('send', 'event', 'login', 'success');}catch(e){console.log(e);}
        showError($translate.instant('form.login_error'));
  		}).finally(function(){
        try{ga('send', 'event', 'login', 'error');}catch(e){console.log(e);}
        $scope.loading = false;
      });
  	};
  });
