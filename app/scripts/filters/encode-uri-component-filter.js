'use strict';

var app = angular.module('beruby');
app.filter('encodeURIComponent', function($window) {
    return $window.encodeURIComponent;
});